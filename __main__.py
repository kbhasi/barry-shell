"""
barry-shell - Home screen for Project Barry
Copyright 2017-2019 Kevin Bhasi.
This file is part of Project Barry.

    Project Barry is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Project Barry is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Project Barry.  If not, see <http://www.gnu.org/licenses/>.

"""
''' Home screen for Project Barry'''
__license__   = 'GPL v3'
#__copyright__ = '2008, Kovid Goyal <kovid@kovidgoyal.net>'
__docformat__ = 'markdown en'
__componentName__ = shell
#__componentVer__ = 0

# EEGG: Well, there he is, Barry the Bookworm, trying to emerge from the ground...
# EEGG: Will he make it? Well, we'll just have to wait.

# TODO: Try to contain the UI in Qt Designer .ui files, unless upstream Calibre does it within its code

class Home(QMainWindow):
    def __init__(self):
        super(Home, self).__init__()
        self.initUI()

    def initUI(self):
        # Window title and icon
        self.setWindowIcon(QIcon.fromTheme('go-home'))
        self.setWindowTitle(socket.gethostname())

        # Check for the presence of certain icons in the active desktop theme before initialising toolbar, otherwise use the "symbolic" variants
        # Codename Barry components should prefer regular icons, and fall back to symbolic icons where needed.
        # An option may be added in the future to allow users to switch those options around
        if QIcon.hasThemeIcon('file-library') == True:
            ShelvesIcon = QIcon.fromTheme('file-library')
        elif QIcon.hasThemeIcon('file-library-symbolic') == True:
            ShelvesIcon = QIcon.fromTheme('file-library-symbolic')
        else:
            pass

        if QIcon.hasThemeIcon('display-brightness') == True:
            BrightnessIcon = QIcon.fromTheme('display-brightness')
        elif QIcon.hasThemeIcon('display-brightness-symbolic') == True:
            BrightnessIcon = QIcon.fromTheme('display-brightness-symbolic')
        else:
            pass

        # Toolbar button definitions
        # TODO: Toolbar layout: Shelves, (flexible spacer), Store, Brightness, Applications, Search, (another flexible spacer), Menu
        ShelvesMenu = QAction(ShelvesIcon, 'Shelves', self)
        # TODO: Add check for "HasLitDisplay" here, referencing barry-branding, otherwise don't show brightness control
        BrightnessControl = QAction(BrightnessIcon, 'Brightness', self)
        ApplicationsMenu = QAction(QIcon.fromTheme('gnome-applications'), 'Applications', self)
        SearchAction = QAction(QIcon.fromTheme('search'), 'Search', self)
        MoreOptionsMenu = QAction(QIcon.fromTheme('application-menu'), 'More Options', self)

        # Top bar itself, internally known as the "marquee"
        marquee = self.addToolBar('Top Bar')
        marquee.addAction(ShelvesMenu)
        # TODO: Add another check for "HasLitDisplay" here, referencing barry-branding, otherwise don't show brightness control
        # BUG: The following 2 toolbar items show up as text, not as icons, at least when running in KDE Plasma (prior to setting up a chroot environment for testing)
        marquee.addAction(BrightnessControl)
        marquee.addAction(ApplicationsMenu)
        marquee.addAction(SearchAction)
        marquee.addAction(MoreOptionsMenu)
        # TODO: Original idea was to have the top bar be fixed, but maybe I might not want to have it be fixed, and instead have users move it around, then reset it after a shell restart.

        # EEGG: Barry himself would be 15 to 18 cm long, with a diameter of 2 cm.
        # Temporary geometry options to at least get the window to open
        # TODO: Find a way to get the window to open maximised, and hopefully with the EMWH classification of a "desktop"
        self.setGeometry(0,0,500,500)
        # EEGG: He's getting close!
        self.show()
        # EEGG: He's made it out of the ground!

# EEGG: Can one guy, help save the GNU/Linux ecosystem from digital deforestation, one line of code at a time?
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Home()
    sys.exit(app.exec_())

## EEGG: 🎵"I'm just a little black cube... of darkness, just a little black cube... of darkness"🎵
