**Development has been temporarily suspended as I found that I needed to go back to my design notes and design a better UI than what I had initially thought.** Development may eventually resume once Calibre 4 has been out for some time, which I believe would use Python 3. Original README file follows.

**TODO: Rewrite code to use PySide instead of PyQt!**

# barry-shell
Home screen for Project Barry

For other information not covered in this README file, please see the repository [wiki](https://gitlab.com/kbhasi/barry-shell/wikis/home). This includes PFAQs (Probably Frequently Asked Questions).

## Dependencies for this component
For version 0.0.2 - 0.0.7:
* python3
  * _more specifically, python3.6_
* ~~python3-pyqt5~~

## Known issues
* A lot of things are missing, which is to be expected for a very early version like this.
* As of this build, it won't run, but should eventually be able to launch, once I copy and paste enough code from Calibre, and tweak that to work with Python 3.x.
* The `barry-branding` module referenced in comments in `__main__.py` is missing, due to me having not deemed the module suitable to be placed in its own GIT repository.
* Toolbar has some missing icons when testing in a KDE Plasma environment. Bug highlighted on line 75 of main script, will fix once I can set up a `chroot` environment.
