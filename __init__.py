#!/usr/bin/python3
"""
barry-shell - Home screen for Project Barry
Copyright 2017-2019 Kevin Bhasi.
This file is part of Project Barry.

    Project Barry is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Project Barry is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Project Barry.  If not, see <http://www.gnu.org/licenses/>.

This file contains the shell initialisation routine, and/or any other code that only needs to run on startup, but it may be subject to change.
"""
""" Import Python modules """
import os, sys
# "socket" module not needed in initialisation routine, only needed in main window to be able to display the hostname as the window title.

# EEGG: Barry, I know you're stressed out about Calibre and Qtile using Python 2, while yoo prefer Python 3, but it's OK. Just cherry-pick lines of code, test, debug, and you should be alright. Just take your time.
if __name__ == '__main__':
    """ Component location awareness.
    This assumes each Project Barry Platform component is in individual directories within specific folders,
    for example: /opt/barry/barry-*, or /usr/lib/barry/barry-* """
    # TODO: Add code to prevent this section from being called if this had been invoked by barry-session
    ## This might work
    this_dir = os.path.dirname(__file__)
    base_dir = os.path.abspath(os.path.join(this_dir, ".."))
    sys.path.insert(0, base_dir)
